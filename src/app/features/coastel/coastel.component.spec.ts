import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoastelComponent } from './coastel.component';

describe('CoastelComponent', () => {
  let component: CoastelComponent;
  let fixture: ComponentFixture<CoastelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoastelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoastelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
