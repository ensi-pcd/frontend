import { City } from "./../models/City";
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { SygicTravelAPIService } from "./../services/sygic-travel-api.service";
import { Media } from "./../models/Media";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-city-medias",
  templateUrl: "./city-medias.component.html",
  styleUrls: ["./city-medias.component.css"]
})
export class CityMediasComponent implements OnInit {
  medias: Media[] = [];
  video: string = null;
  city: City = null;
  constructor(
    private route: ActivatedRoute,
    private sygic: SygicTravelAPIService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.sygic.getPlaceMedia(params.get("id"))
        )
      )
      .subscribe(
        medias => {
          this.medias = medias.map((media: Media) => {
            media.$url_template = media.$url_template.replace(
              "{size}",
              "400x400"
            );
            if (media.$type == "video360" || media.$type == "video")
              this.video = media.$url;
            return media;
          });
        },
        error => console.log(error)
      );

    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => this.sygic.getPlace(params.get("id")))
      )
      .subscribe(
        (city: City) => {
          this.city = city;
          console.log(this.city);
        },
        error => console.log(error)
      );
  }

  onClick(lien){
   // this.route.navigate(["/hotels"],lien);
  }

  @ViewChild("videoPlayer") videoplayer: ElementRef;

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
}
