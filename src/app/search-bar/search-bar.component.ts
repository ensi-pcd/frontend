import { SygicTravelAPIService } from "../services/sygic-travel-api.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NotificationService } from "../services/notification.service";

@Component({
  selector: "app-search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.css"]
})
export class SearchBarComponent implements OnInit {
  cities;
  filtredCities;
  cityName;

  constructor(
    private route: Router,
    private notification: NotificationService,
    private sygic: SygicTravelAPIService
  ) {}

  ngOnInit() {
    //load cities names from file
    this.sygic.getCities().subscribe(cities => {
      this.cities = cities;
    });
  }
  onkeyup(event: any) {
    //cities not loaded yet from file
    if (!this.cities) return;

    //this.value = event.target.value;
    if (!this.cityName || this.cityName.trim() == "") {
      this.filtredCities = null;
      return;
    }
    this.filtredCities = this.cities
      .filter(
        city =>
          city.name
            .trim()
            .toUpperCase()
            .includes(this.cityName.trim().toUpperCase()) &&
          //same first caractere
          city.name.trim()[0].toUpperCase() ==
            this.cityName.trim()[0].toUpperCase()
      )
      //show just 5 cities
      .filter((city, index) => index < 5);
  }

  cityClicked(cityInTheList) {
    this.cityName = cityInTheList;
    this.visit();
  }

  visit() {
    if (!this.cityName || this.cityName.trim() == "") {
      this.notification.error("you must write a city !");
      return;
    }

    let foundCity = this.cities.find(
      city => this.cityName.toUpperCase() == city.name.toUpperCase()
    );
    if (!foundCity) {
      this.notification.error("no city with this name");
      return;
    }

    this.route.navigate(["/details", foundCity]);
  }
}
