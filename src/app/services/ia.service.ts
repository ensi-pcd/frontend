import { of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class IaService {
  url = "http://localhost:5000/";
  constructor(private http: HttpClient) {}

  getTopCities(continent: number, budget: number, isCoastel: number) {
    // if (!continent || !budget || !isCoastel) return of(null);
    if (continent < 0 || continent > 50000) return of(null);
    if (budget < 0 || budget > 20000) return of(null);
    if (isCoastel < 0 || isCoastel > 1) return of(null);
    return this.http.get(this.url + continent + "/" + budget + "/" + isCoastel);
  }
}
