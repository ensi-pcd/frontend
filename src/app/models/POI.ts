import { Media } from "./Media";

export class POI {
  // Popularity rating of the place in range <0-10>. Higher rating means higher popularity.
  private id: string;
  private level: string;
  private categories: string[];
  private rating: number;
  private location: { lat: number; lng: number };
  private bounding_box: any;
  private name: string;
  private perex: string;
  private marker: string;
  private url: string;
  private thumbnail_url: string;
  private country: string;
  private continent: string;
  private duration: string;
  private customer_rating: string;
  private star_rating: string;
  //wikidataId: string;

  constructor(
    $id: string,
    $level: string,
    $categories: string[],
    $rating: number,
    $name: string,
    $perex: string,
    $marker: string,
    $country: string,
    $continent: string,
    $url: string,
    $thumbnail_url: string,
    $location: any,
    $bounding_box: any,
    $duration: string,
    $customer_rating: string,
    $star_rating: string
  ) {
    this.id = $id;
    this.level = $level;
    this.categories = $categories;
    this.rating = $rating;
    this.name = $name;
    this.perex = $perex;
    this.marker = $marker;
    this.country = $country;
    this.continent = $continent;
    this.url = $url;
    this.thumbnail_url = $thumbnail_url;
    this.location = $location;
    this.bounding_box = $bounding_box;
    this.duration = $duration;
    this.star_rating = $star_rating;
    this.customer_rating = $customer_rating;
  }
}
