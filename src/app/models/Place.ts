import { Media } from "./Media";
export abstract class Place {
  private id: string;
  private level: string; //enum["continent", "country", "state", "region", "county", "city", "town", "village", "settlement", "locality", "neighbourhood", "archipelago", "island", "poi"]
  // See section Place categories.
  private categories: string; //["discovering", "eating", "going_out", "hiking", "playing", "relaxing", "shopping", "sightseeing", "sleeping", "doing_sports", "traveling"] 0…∞

  private name: string;
  //Beginning of place description, up to 160 characters.
  private perex: string | null;
  private description: string | null;

  private url: string | null;
  //URL of place overview on Sygic Travel. Custom Places do not have an url.

  private thumbnail_url: string | null;
  // Image of place in dimensions 150×150 pixels.

  private marker: string;
  // Name of the suggested marker icon. Each part after ‚:‘ character makes it more specific. You can use only prefix of the name with more common icon. Default value is default.
  private country: string;
  private continent: string;
  // Array of IDs of parent places.

  //Area of the place in square meters
  private area: number | null;
  private tags: string[];

  private media_count: number;
  private video_preview: string | null;
  private media: Media[];

  //Number of collections for place in given language (collections that have the place as parent).
  private collection_count: number;

  //private wikidataId: string | null;
  //See section Geographical attributes.
  private quadkey: string;

  // See section Geographical attributes.
  private location: {
    lat: number;
    lng: number;
  };
  //See section Geographical attributes.
  private bounding_box: {
    south: number;
    west: number;
    north: number;
    east: number;
  } | null;

  constructor(
    $id: string,
    $level: string,
    $categories: string,
    $name: string,
    $perex: string,
    $description: string,
    $url: string,
    $thumbnail_url: string,
    $marker: string,
    $country: string,
    $continent: string,
    $area: number,
    $tags: string[],
    $media_count: number,
    $video_preview: string,
    $media: Media[],
    $collection_count: number,
    //$wikidataId: string,
    $location: any,
    $bounding_box: any
  ) {
    this.id = $id;
    this.name = $name;
    this.level = $level;
    this.categories = $categories;
    this.perex = $perex;
    this.description = $description;
    this.url = $url;
    this.thumbnail_url = $thumbnail_url;
    this.marker = $marker;
    this.country = $country;
    this.continent = $continent;
    this.area = $area;
    this.tags = $tags;
    this.media_count = $media_count;
    this.video_preview = $video_preview;
    this.media = $media;
    this.collection_count = $collection_count;
    //this.wikidataId = $wikidataId;
    (this.location = $location), (this.bounding_box = $bounding_box);
  }

  /**
   * Getter $id
   * @return {string}
   */
  public get $id(): string {
    return this.id;
  }

  /**
   * Setter $id
   * @param {string} value
   */
  public set $id(value: string) {
    this.id = value;
  }

  /**
   * Getter $level
   * @return {string}
   */
  public get $level(): string {
    return this.level;
  }

  /**
   * Setter $level
   * @param {string} value
   */
  public set $level(value: string) {
    this.level = value;
  }

  /**
   * Getter $categories
   * @return {string}
   */
  public get $categories(): string {
    return this.categories;
  }

  /**
   * Setter $categories
   * @param {string} value
   */
  public set $categories(value: string) {
    this.categories = value;
  }

  /**
   * Getter $name
   * @return {string}
   */
  public get $name(): string {
    return this.name;
  }

  /**
   * Setter $name
   * @param {string} value
   */
  public set $name(value: string) {
    this.name = value;
  }

  /**
   * Getter $perex
   * @return {string }
   */
  public get $perex(): string {
    return this.perex;
  }

  /**
   * Setter $perex
   * @param {string } value
   */
  public set $perex(value: string) {
    this.perex = value;
  }

  /**
   * Getter $description
   * @return {string }
   */
  public get $description(): string {
    return this.description;
  }

  /**
   * Setter $description
   * @param {string } value
   */
  public set $description(value: string) {
    this.description = value;
  }

  /**
   * Getter $url
   * @return {string }
   */
  public get $url(): string {
    return this.url;
  }

  /**
   * Setter $url
   * @param {string } value
   */
  public set $url(value: string) {
    this.url = value;
  }

  /**
   * Getter $thumbnail_url
   * @return {string }
   */
  public get $thumbnail_url(): string {
    return this.thumbnail_url;
  }

  /**
   * Setter $thumbnail_url
   * @param {string } value
   */
  public set $thumbnail_url(value: string) {
    this.thumbnail_url = value;
  }

  /**
   * Getter $marker
   * @return {string}
   */
  public get $marker(): string {
    return this.marker;
  }

  /**
   * Setter $marker
   * @param {string} value
   */
  public set $marker(value: string) {
    this.marker = value;
  }

  /**
   * Getter $country
   * @return {string}
   */
  public get $country(): string {
    return this.country;
  }

  /**
   * Setter $country
   * @param {string} value
   */
  public set $country(value: string) {
    this.country = value;
  }

  /**
   * Getter $continent
   * @return {string}
   */
  public get $continent(): string {
    return this.continent;
  }

  /**
   * Setter $continent
   * @param {string} value
   */
  public set $continent(value: string) {
    this.continent = value;
  }

  /**
   * Getter $area
   * @return {number }
   */
  public get $area(): number {
    return this.area;
  }

  /**
   * Setter $area
   * @param {number } value
   */
  public set $area(value: number) {
    this.area = value;
  }

  /**
   * Getter $tags
   * @return {string[]}
   */
  public get $tags(): string[] {
    return this.tags;
  }

  /**
   * Setter $tags
   * @param {string[]} value
   */
  public set $tags(value: string[]) {
    this.tags = value;
  }

  /**
   * Getter $media_count
   * @return {number}
   */
  public get $media_count(): number {
    return this.media_count;
  }

  /**
   * Setter $media_count
   * @param {number} value
   */
  public set $media_count(value: number) {
    this.media_count = value;
  }

  /**
   * Getter $video_preview
   * @return {string }
   */
  public get $video_preview(): string {
    return this.video_preview;
  }

  /**
   * Setter $video_preview
   * @param {string } value
   */
  public set $video_preview(value: string) {
    this.video_preview = value;
  }

  /**
   * Getter $media
   * @return {Media[]}
   */
  public get $media(): Media[] {
    return this.media;
  }

  /**
   * Setter $media
   * @param {Media[]} value
   */
  public set $media(value: Media[]) {
    this.media = value;
  }

  /**
   * Getter $collection_count
   * @return {number}
   */
  public get $collection_count(): number {
    return this.collection_count;
  }

  /**
   * Setter $collection_count
   * @param {number} value
   */
  public set $collection_count(value: number) {
    this.collection_count = value;
  }


  //   /**
  //    * Getter $wikidataId
  //    * @return {string }
  //    */
  //   public get $wikidataId(): string {
  //     return this.wikidataId;
  //   }

  //   /**
  //    * Setter $wikidataId
  //    * @param {string } value
  //    */
  //   public set $wikidataId(value: string) {
  //     this.wikidataId = value;
  //   }
}
