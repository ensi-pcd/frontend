import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityMediasComponent } from './city-medias.component';

describe('CityMediasComponent', () => {
  let component: CityMediasComponent;
  let fixture: ComponentFixture<CityMediasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityMediasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityMediasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
