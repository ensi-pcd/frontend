import { Media } from "./../models/Media";
import { DetailedPOI } from "./../models/DetailedPOI";
import { switchMap } from "rxjs/operators";
import { ParamMap } from "@angular/router";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { SygicTravelAPIService } from "./../services/sygic-travel-api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-poi-detail",
  templateUrl: "./poi-detail.component.html",
  styleUrls: ["./poi-detail.component.css"]
})
export class PoiDetailComponent implements OnInit {
  poi: DetailedPOI = null;
  medias: Media[];
  constructor(
    private route: ActivatedRoute,
    private sygic: SygicTravelAPIService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => this.sygic.getPoi(params.get("id")))
      )
      .subscribe((poi: DetailedPOI) => {
        this.poi = poi;
      });

    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.sygic.getPlaceMedia(params.get("id"))
        )
      )
      .subscribe((medias: Media[]) => {
        this.medias = medias.map((media: Media) => {
          media.$url_template = media.$url_template.replace(
            "{size}",
            "400x400"
          );
          return media;
        });
      });
  }
}
