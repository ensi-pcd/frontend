import { FeaturesService } from "../../services/features.service";
import { FeaturesComponent } from "../features.component";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-coastel",
  templateUrl: "./coastel.component.html",
  styleUrls: ["./coastel.component.css"]
})
export class CoastelComponent implements OnInit {
  selected = false;
  constructor(private featuresService: FeaturesService) {}
  ngOnInit() {}

  yes() {
    this.featuresService.setCoastel(1);
    this.selected = true;
  }

  no() {
    this.featuresService.setCoastel(0);
    this.selected = false;
  }
}
