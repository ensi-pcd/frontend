import { Place } from "./Place";

export class City extends Place {
  constructor(
    id,
    $level,
    $categories,
    $name,
    $perex,
    $description,
    $url,
    $thumbnail_url,
    $marker,
    $country,
    $continent,
    $area,
    $tags,
    $media_count,
    $video_preview,
    $media,
    $collection_count,
    //  $wikidataId
    $location,
    $bobounding_box
  ) {
    super(
      id,
      $level,
      $categories,
      $name,
      $perex,
      $description,
      $url,
      $thumbnail_url,
      $marker,
      $country,
      $continent,
      $area,
      $tags,
      $media_count,
      $video_preview,
      $media,
      $collection_count,
      //$wikidataId,
      $location,
      $bobounding_box
    );
  }
}
