import { SygicTravelAPIService } from "./../services/sygic-travel-api.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-city-description",
  templateUrl: "./city-description.component.html",
  styleUrls: ["./city-description.component.css"]
})
export class CityDescriptionComponent implements OnInit {
  city$;
  city;
  constructor(
    private route: ActivatedRoute,
    private sygic: SygicTravelAPIService,
    private router: Router
  ) {}

  ngOnInit() {
    this.city$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => this.sygic.getPlace(params.get("id")))
      )
      .subscribe(
        city => {
          this.city = city;
          console.log(this.city);
        },
        error => console.log(error)
      );
  }

  discover_it() {
    //if city not loaded yet out 
    if (!this.city) return;
    this.router.navigate(["/details", { id: this.city.id }]);
  }
}
