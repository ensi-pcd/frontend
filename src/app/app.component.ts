import { NotificationService } from "./services/notification.service";
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(private notification: NotificationService) {}

  showToaster() {
    this.notification.success("bonjour");
  }
}
