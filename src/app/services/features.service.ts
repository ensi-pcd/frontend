import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class FeaturesService {
  budget: number = 1000;
  continent: number = 10000;
  isCoastel: number = 0;

  constructor(private route: Router) {}

  setBudget(newBudget: number) {
    this.budget = newBudget;
  }

  setContinent(newContinent: number) {
    this.continent = newContinent;
  }

  setCoastel(newCoastel: number) {
    this.isCoastel = newCoastel;
  }

  navigateToTopCities() {
    this.route.navigate([
      "/top",
      {
        budget: this.budget,
        continent: this.continent,
        isCoastel: this.isCoastel
      }
    ]);
  }

  /*
  budgetSubject = new Subject<number>();

  changeBudget(newBudget: number) {
    this.budget = newBudget;
    this.emitBudget();
  }

  emitBudget() {
    this.budgetSubject.next(this.budget);
  }

  continentSubject = new Subject<string>();

  changeContinent(newContinent: string) {
    this.continent = newContinent;
    this.emitContinent();
  }

  emitContinent() {
    this.continentSubject.next(this.continent);
  }*/
}
