import { Subscription } from "rxjs";
import { NotificationService } from "./notification.service";
import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class WikidataService {
  urlCity = "http://api.haykranen.nl/wikidata/entity?q=";
  urlCities = "../assets/citiesRecIdName.json";

  city: any[] = [];
  cities: any[];

  constructor(
    private http: HttpClient,
    private notification: NotificationService
  ) {
    //get cities from file
    this.http.get(this.urlCities).subscribe((cities: any[]) => {
      this.cities = cities;
    });
  }

  //for search bar service
  //return observable array of cities name
  getCities(): Observable<any> {
    return this.http.get(this.urlCities);
    //  .pipe(map((data: any[]) => data.map((city: any) => city.name)));
  }

  getData(cityId: String): Observable<any> {
    let image: String;
    let haveWater;
    let result;
    return this.http.get(this.urlCity + cityId.toString()).pipe(
      map(
        (data: any) => {
          if (data.error) return null;

          let city = data.response[Object.keys(data.response)[0]].claims;

          for (let i = 0; i < city.length; i++) {
            if (city[i].property_id == "P18")
              image = city[i].values[0].image.full.split(" ").join("_");
            if (city[i].property_id == "P206") haveWater = true;
          }
          result = image;
          //return this.sanitizer.bypassSecurityTrustUrl(result.toString());
          return result;
        },
        error => null
      )
    );
  }
}
