import { Media } from "./Media";
import { Place } from "./Place";

export class DetailedPOI extends Place {
  // Popularity rating of the place in range <0-10>. Higher rating means higher popularity.
  private star_rating: number | null;
  // Rating in stars from 0 to 5.
  private customer_rating: number | null;
  //The customer rating on scale from 0 to 10 (the higher the better).
  private city: string;
  private rating: number;
  private duration: number | null;
  // Expected duration of visit in seconds.
  private is_deleted: boolean;
  private address: string | null;
  private email: string | null;
  private opening_hours: string | null;
  private phone: string | null;
  private admission: string | null;
  // Currency of reference price.
  private currency: string | null;
  //Linked product price.
  private price: number | null;

  constructor(
    // Place
    $id: string,
    $level: string,
    $categories: string,
    $name: string,
    $perex: string,
    $description: string,
    $url: string,
    $thumbnail_url: string,
    $marker: string,
    $country: string,
    $continent: string,
    $area: number,
    $tags: string[],
    $media_count: number,
    $video_preview: string,
    $media: Media[],
    $collection_count: number,
    //$wikidataId: string,
    $location: any,
    $bounding_box: any,
    //POI
    $star_rating: number,
    $customer_rating: number,
    $city: string,
    $rating: number,
    $duration: number,
    $is_deleted: boolean,
    $address: string,
    $email: string,
    $opening_hours: string,
    $phone: string,
    $admission: string,
    $currency: string,
    $price: number
  ) {
    super(
      $id,
      $level,
      $categories,
      $name,
      $perex,
      $description,
      $url,
      $thumbnail_url,
      $marker,
      $country,
      $continent,
      $area,
      $tags,
      $media_count,
      $video_preview,
      $media,
      $collection_count,
      //$wikidataId,
      $location,
      $bounding_box
    );

    this.star_rating = $star_rating;
    this.customer_rating = $customer_rating;
    this.city = $city;
    this.rating = $rating;
    this.duration = $duration;
    this.is_deleted = $is_deleted;
    this.address = $address;
    this.email = $email;
    this.opening_hours = $opening_hours;
    this.phone = $phone;
    this.admission = $admission;
    this.currency = $currency;
    this.price = $price;
  }
}
