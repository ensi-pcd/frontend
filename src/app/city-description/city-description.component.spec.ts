import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityDescriptionComponent } from './city-description.component';

describe('CityDescriptionComponent', () => {
  let component: CityDescriptionComponent;
  let fixture: ComponentFixture<CityDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
