import { DetailedPOI } from "../models/DetailedPOI";
import { City } from "../models/City";
import { Media } from "../models/Media";
import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map, catchError, tap } from "rxjs/operators";
import { POI } from "../models/POI";

const citiesEndpoint = "../assets/cities.json";
const endpoint = "https://api.sygictravelapi.com/1.1/en";
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "x-api-key": "w1sM8mNUKZ3eOwPNXEiby7miCLezxzRA4JfQg7eY"
  })
};

// const endpoint = "";
// const httpOptions = {};
const endpointCity = "../assets/city.json";
const endpointMedia = "../assets/medias.json";
const endpointPlaces = "../assets/restos.json";
const endpointpoi = "../assets/DetailHotel.json";

@Injectable({
  providedIn: "root"
})
export class SygicTravelAPIService {
  // inject the HttpClient module
  constructor(private http: HttpClient) {}

  // extracting the response
  private extractData(res: any) {
    let body = res.data;
    return body || {};
  }

  // handler for errors
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  ////////////////////////////////////////////////////////////////////////////////////////////

  //cityId : city:1
  //places :resaturants , hotels ...
  //categories : discovering, eating, going_out, hiking, playing, relaxing, shopping, sightseeing, sleeping, doing_sports, traveling
  //caegorie can be like this  : eating:restaurant:asian
  getPlaces(cityId, cat, max): Observable<any> {
    return this.http
      .get(
        endpoint +
          "/places/list?parents=" +
          cityId +
          "&categories=" +
          cat +
          "&limit=" +
          max,
        httpOptions
      )
      .pipe(
        map(setPlace),
        catchError(this.handleError<any>("getPlaces"))
      );
  }

  getPlace(id): Observable<any> {
    return this.http.get(endpoint + "/places/" + id, httpOptions).pipe(
      map(setCity),
      catchError(this.handleError<any>("getPlace"))
    );
  }

  getPoi(id): Observable<any> {
    return this.http.get(endpointpoi /* + "/places/" + id, httpOptions*/).pipe(
      map(setPoi),
      catchError(this.handleError<any>("getPoi"))
    );
  }

  getPlaceMedia(id): Observable<any> {
    return this.http
      .get(endpoint + "/places/" + id + "/media", httpOptions)
      .pipe(
        map(setMedias),
        catchError(this.handleError<any>("getPlaceMedia"))
      );
  }

  getTours(city): Observable<any> {
    return this.http
      .get(endpoint + "/tours/viator?parent_place_id=" + city, httpOptions)
      .pipe(
        map(this.extractData),
        catchError(this.handleError<any>("getTours"))
      );
  }

  getTrips(city): Observable<any> {
    return this.http
      .get(endpoint + "/trips/templates?parent_place_id=" + city, httpOptions)
      .pipe(
        map(this.extractData),
        catchError(this.handleError<any>("getTrips"))
      );
  }

  getTrip(id): Observable<any> {
    return this.http.get(endpoint + "/trips/" + id, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError<any>("getTrip"))
    );
  }

  //for the search-bar component
  //return observable array of cities => format  [{"id":"city:1","name":"London","country_id":"country:1"} ,{}, ...]
  getCities(): Observable<any> {
    return this.http.get(citiesEndpoint);
    //  .pipe(map((data: any[]) => data.map((city: any) => city.name)));
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//              transform data from sygic api to our classes : POI , City , Place , DetailedPOI

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//transform city from sygic to City class
//forwho : city , medias => if city direct a9ra => medias => media= media.data.media
let setMedias = (media: any, forwho): Media[] => {
  if (!forwho) media = media.data.media;
  return media.map(
    el =>
      new Media(
        el.id,
        el.type,
        el.url,
        el.url_template,
        el.attribution.title,
        el.created_at,
        el.location
      )
  );
};

//parent_ids  is an array : ["city:4228","region:4","country:13","continent:1"],
//transform city from sygic api to City class
let setCity = (city: any): City => {
  city = city.data.place;
  // city = city.place;
  //complex property that can be null from  sygic api
  let media = city.main_media ? setMedias(city.main_media.media, "city") : null;
  let country = city.parent_ids.find(el => el.includes("country"));
  let continent = city.parent_ids.find(el => el.includes("continent"));
  let tags = city.tags.map(el => el.name);
  let media_count = city.main_media ? city.media_count : null;
  let video_preview = city.main_media
    ? city.main_media.usage.video_preview
    : null;
  let description = city.description ? city.description.text : null;

  return new City(
    city.id,
    city.level,
    city.categories,
    city.name,
    city.perex,
    description,
    city.url,
    city.thumbnail_url,
    city.marker,
    country,
    continent,
    city.area,
    tags,
    media_count,
    video_preview,
    media,
    city.collection_count,
    city.location,
    city.bounding_box
  );
};

//get top poi from a city and transform to array of POI
//parcours places
let setPlace = (places: any): POI[] => {
  places = places.data.places;
  return places.map(
    (poi: any) =>
      new POI(
        poi.id,
        poi.level,
        poi.categories,
        poi.rating,
        poi.name,
        poi.perex,
        poi.marker,
        poi.parent_ids.find(el => el.includes("country")),
        poi.parent_ids.find(el => el.includes("continent")),
        poi.url,
        poi.thumbnail_url,
        poi.location,
        poi.bounding_box,
        poi.duration,
        poi.customer_rating,
        poi.star_rating
      )
  );
};

let setPoi = (poi: any): DetailedPOI => {
  poi = poi.data.place;
  let media = poi.main_media ? setMedias(poi.main_media.media, "poi") : null;
  let country = poi.parent_ids.find(el => el.includes("country"));
  let continent = poi.parent_ids.find(el => el.includes("continent"));
  let tags = poi.tags.map(el => el.name);
  let media_count = poi.main_media ? poi.media_count : null;
  let video_preview = poi.main_media
    ? poi.main_media.usage.video_preview
    : null;
  let description = poi.description ? poi.description.text : null;
  return new DetailedPOI(
    poi.id,
    poi.level,
    poi.categories,
    poi.name,
    poi.perex,
    description,
    poi.url,
    poi.thumbnail_url,
    poi.marker,
    country,
    continent,
    poi.area,
    tags,
    media_count,
    null,
    media,
    poi.collection_count,
    poi.location,
    poi.bounding_box,
    poi.star_rating,
    poi.customer_rating,
    null,
    poi.rating,
    poi.duration,
    poi.is_deleted,
    poi.address,
    poi.email,
    poi.opening_hours,
    poi.phone,
    poi.admission,
    null,
    null
  );
};
