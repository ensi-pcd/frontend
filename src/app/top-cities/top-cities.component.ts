import { SygicTravelAPIService } from "./../services/sygic-travel-api.service";
import { ParamMap } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { IaService } from "./../services/ia.service";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-top-cities",
  templateUrl: "./top-cities.component.html",
  styleUrls: ["./top-cities.component.css"]
})
export class TopCitiesComponent implements OnInit {
  display: string[] = [
    "col-md-6",
    "col-md-6",
    "col-md-4",
    "col-md-8",
    "col-md-6",
    "col-md-3",
    "col-md-3",
    "col-md-4",
    "col-md-4",
    "col-md-4"
  ];
  cities: string[] = [];
  constructor(
    private route: ActivatedRoute,
    private IA: IaService,
    private router: Router,
    private sygic: SygicTravelAPIService
  ) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.IA.getTopCities(
            +params.get("continent"),
            +params.get("budget"),
            +params.get("isCoastel")
          )
        )
      )
      .subscribe(data => {
        //cities from ia server
        this.cities = data;
      });
  }
}
