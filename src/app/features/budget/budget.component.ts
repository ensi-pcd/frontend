import { FeaturesService } from "../../services/features.service";
import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-budget",
  templateUrl: "./budget.component.html",
  styleUrls: ["./budget.component.css"]
})
export class BudgetComponent implements OnInit {
  range: number = 1500;
  constructor(private featureService: FeaturesService) {}
  ngOnInit() {}
  valueChanged(e) {
    this.range = +e;
    this.featureService.setBudget(this.range);
  }
}
