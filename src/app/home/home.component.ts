import { Router } from '@angular/router';
import { SygicTravelAPIService } from "../services/sygic-travel-api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(private sygic: SygicTravelAPIService, private route: Router) {}
  city;
  places;
  ngOnInit() {
    // this.sygic.getPlace("city:1").subscribe(data => console.log(data));
    /* this.sygic
      .getPlaces("city:10", "eating", "100")
      .subscribe(data => console.log(data));*/
  }

  visit(id){
     this.route.navigate(['/details', {id : 'city:'+id}])
  }
}
