import { TestBed } from '@angular/core/testing';

import { SygicTravelAPIService } from './sygic-travel-api.service';

describe('SygicTravelAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SygicTravelAPIService = TestBed.get(SygicTravelAPIService);
    expect(service).toBeTruthy();
  });
});
