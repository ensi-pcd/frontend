import { TopCitiesComponent } from "./top-cities/top-cities.component";
import { CityDescriptionComponent } from "./city-description/city-description.component";
import { FooterComponent } from "./footer/footer.component";
import { ContinentComponent } from "./features/continent/continent.component";
import { BudgetComponent } from "./features/budget/budget.component";
import { CityDetailsComponent } from "./city-details/city-details.component";
import { HomeComponent } from "./home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CityMediasComponent } from "./city-medias/city-medias.component";
import { PoiDetailComponent } from "./poi-detail/poi-detail.component";
import { CoastelComponent } from "./features/coastel/coastel.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      { path: "", redirectTo: "budget", pathMatch: "full" },
      { path: "budget", component: BudgetComponent },
      { path: "continent", component: ContinentComponent },
      { path: "isCoastel", component: CoastelComponent }
    ]
  },
  { path: "details", component: CityMediasComponent },
  {
    path: "description",
    component: CityDescriptionComponent
  },
  {
    path: "city",
    component: CityDetailsComponent
  },
  {
    path: "poi",
    component: PoiDetailComponent
  },
  {
    path: "top",
    component: TopCitiesComponent
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
