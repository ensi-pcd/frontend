import { POI } from "../models/POI";
import { SygicTravelAPIService } from "../services/sygic-travel-api.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-city-details",
  templateUrl: "./city-details.component.html",
  styleUrls: ["./city-details.component.css"]
})
export class CityDetailsComponent implements OnInit {
  city;
  pois: POI[];
  constructor(
    private route: ActivatedRoute,
    private sygic: SygicTravelAPIService,
    private router: Router
  ) {}

  rating(by) {}

  map() {
    window.open(this.city.url);
  }

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          let type;

          return this.sygic.getPlaces(
            params.get("id"),
            params.get("type"),
            "10"
          );
          // this.sygic.getPlaces(4, "sleeping", "4")
        })
      )
      .subscribe(
        pois => {
          this.pois = pois.filter((el, i) => i < 10);
          console.log(pois);
          // .filter((el, i) => i < 5);
        },
        error => console.log(error)
      );

    this.route.paramMap
      .pipe(
        switchMap(
          (params: ParamMap) => this.sygic.getPlace(params.get("id"))
          // this.sygic.getPlaces(4, "sleeping", "4")
        )
      )
      .subscribe(
        city => {
          this.city = city;
        },
        error => console.log(error)
      );
  }
}
