import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCitiesItemComponent } from './top-cities-item.component';

describe('TopCitiesItemComponent', () => {
  let component: TopCitiesItemComponent;
  let fixture: ComponentFixture<TopCitiesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopCitiesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopCitiesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
