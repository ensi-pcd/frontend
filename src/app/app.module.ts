import { AngularFontAwesomeModule } from "angular-font-awesome";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { FooterComponent } from "./footer/footer.component";
import { HomeComponent } from "./home/home.component";
import { CityDetailsComponent } from "./city-details/city-details.component";
import { FeaturesComponent } from "./features/features.component";
import { BudgetComponent } from "./features/budget/budget.component";
import { ContinentComponent } from "./features/continent/continent.component";
import { TopCitiesComponent } from "./top-cities/top-cities.component";
import { TopCitiesItemComponent } from "./top-cities-item/top-cities-item.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { CityDescriptionComponent } from "./city-description/city-description.component";
import { SearchBarComponent } from "./search-bar/search-bar.component";
import { FormsModule } from "@angular/forms";
import { CityMediasComponent } from "./city-medias/city-medias.component";
import { PoiDetailComponent } from "./poi-detail/poi-detail.component";
import { CoastelComponent } from "./features/coastel/coastel.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    CityDetailsComponent,
    FeaturesComponent,
    BudgetComponent,
    ContinentComponent,
    TopCitiesComponent,
    TopCitiesItemComponent,
    CityDescriptionComponent,
    SearchBarComponent,
    CityMediasComponent,
    PoiDetailComponent,
    CoastelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
