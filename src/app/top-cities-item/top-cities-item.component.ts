import { SygicTravelAPIService } from "./../services/sygic-travel-api.service";
import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-top-cities-item",
  templateUrl: "./top-cities-item.component.html",
  styleUrls: ["./top-cities-item.component.css", "../../assets/materialise.css"]
})
export class TopCitiesItemComponent implements OnInit {
  @Input() display;
  @Input() city;
  name;
  country;
  image;

  constructor(private sygic: SygicTravelAPIService, private route: Router) {}

  visit() {
    this.route.navigate(["/details", this.city.id]);
  }

  ngOnInit() {
    this.name = this.city.name;
    this.country = this.city.country;
    this.sygic.getPlaceMedia(this.city.id).subscribe(data => {
      if (data && data[0])
        this.image = data[0].url_template.replace("{size}", "600x600");

      /*if (data.media) this.image = data.media[0].url;
      this.name = data.name;*/
    });
  }
}
