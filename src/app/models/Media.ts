export class Media {
  private id: string;
  private type: string; //  ["photo", "photo360", "video", "video360"]
  private url: string;
  private url_template: string;
  private title: string | null;
  private created_at: string;

  /* TODO class location */
  private location: {
    lat: number;
    lng: number;
  } | null;

  constructor(
    $id: string,
    $type: string,
    $url: string,
    $url_template: string,
    $title: string,
    $created_at: string,
    $location: any
  ) {
    this.id = $id;
    this.type = $type;
    this.url = $url;
    this.title = $title;
    this.created_at = $created_at;
    this.location = $location;
    this.url_template = $url_template;
  }
  /**
   * Getter $id
   * @return {string}
   */
  public get $id(): string {
    return this.id;
  }

  /**
   * Setter $id
   * @param {string} value
   */
  public set $id(value: string) {
    this.id = value;
  }

  /**
   * Getter $url
   * @return {string}
   */
  public get $url(): string {
    return this.url;
  }

  /**
   * Setter $url
   * @param {string} value
   */
  public set $url(value: string) {
    this.url = value;
  }

  /**
   * Getter $url_template
   * @return {string}
   */
  public get $url_template(): string {
    return this.url_template;
  }

  /**
   * Setter $url_template
   * @param {string} value
   */
  public set $url_template(value: string) {
    this.url_template = value;
  }

  /**
   * Getter $type
   * @return {string}
   */
  public get $type(): string {
    return this.type;
  }

  /**
   * Setter $type
   * @param {string} value
   */
  public set $type(value: string) {
    this.type = value;
  }

  /**
   * Getter $title
   * @return {string }
   */
  public get $title(): string {
    return this.title;
  }

  /**
   * Setter $title
   * @param {string } value
   */
  public set $title(value: string) {
    this.title = value;
  }

  /**
   * Getter $created_at
   * @return {string}
   */
  public get $created_at(): string {
    return this.created_at;
  }

  /**
   * Setter $created_at
   * @param {string} value
   */
  public set $created_at(value: string) {
    this.created_at = value;
  }
}
