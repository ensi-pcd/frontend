import { FeaturesService } from "../../services/features.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-continent",
  templateUrl: "./continent.component.html",
  styleUrls: ["./continent.component.css"]
})
export class ContinentComponent {
  continent: string = "europe";

  constructor(private featureService: FeaturesService) {}

  onContinentClick(name: string, continent: number) {
    this.continent = name;
    this.featureService.setContinent(continent);
  }
}
