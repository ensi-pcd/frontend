import { FeaturesService } from "../services/features.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

@Component({
  selector: "app-features",
  templateUrl: "./features.component.html",
  styleUrls: ["./features.component.css"]
})
export class FeaturesComponent {
  constructor(private featuresService: FeaturesService) {}

  onClick() {
    this.featuresService.navigateToTopCities();
  }

  /*BudgetSubscription: Subscription;


  ngOnInit() {
    this.BudgetSubscription = this.featuresService.budgetSubject.subscribe(
      (newBudget: number) => {
        this.budget = newBudget;
      }
    );
    //emit budget value at the first time
    this.featuresService.emitBudget();
  }

  ngOnDestroy() {
    this.BudgetSubscription.unsubscribe();
  }*/
}
